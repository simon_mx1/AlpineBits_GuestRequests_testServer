<?php

namespace AlpineBits\functionalTests;


use \AlpineBits\Tests\BaseFunctions;
use \AlpineBits\Actions\OTA_Read\GuestRequests\RQExtractor;


class ExtractorSingleTest extends BaseFunctions
{

	public function testExtractorHotelCode()
	{
		$extractor = new RQExtractor("/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml");
		$this -> assertEquals('123', $extractor -> getHotelCode());
	}

	public function testExtractorHotelName()
	{
		$extractor = new RQExtractor("/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml");
		$this -> assertEquals('Frangart Inn', $extractor -> getHotelName());
	}

	public function testExtractorStartDate()
	{
		$extractor = new RQExtractor("/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml");

		$xml = \simplexml_load_file("/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml");

		//var_dump($xml -> ReadRequest);

		$this -> assertEquals($xml -> ReadRequests -> HotelReadRequest -> SelectionCriteria -> attributes() -> Start, $extractor -> getStartDate());
	}

}