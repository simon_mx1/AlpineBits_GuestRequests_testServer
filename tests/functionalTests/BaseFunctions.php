<?php

namespace AlpineBits\Tests;

use GuzzleHttp;

abstract class BaseFunctions extends \PHPUnit\Framework\TestCase
{

	const URL_BASE = "http://localhost:8080/AlpineBits";

	protected function request($action, $request = "", $auth = ['Test', 'Test'], $XAlpineBitsClientID = '2017-10')
	{

		$client = $this -> getClient();

		$output = $client -> post('AlpineBits/public/',
			[
				'multipart' => [
					[
						'name' => 'action',
						'contents' => $action
					],
					[
						'name' => 'request',
						'contents' => $request
					]
				],
				'auth' => $auth,
				'headers' => array_filter([
						'X-AlpineBits-ClientID' => $XAlpineBitsClientID,

					]

				),
				'http_errors' => false
			]
		);

		return ['body' => $output -> getBody() -> getContents(),
			'statuscode' => $output -> getStatusCode(),
			'contentType' => $output -> getHeader("content-type")];
	}

	private function getClient()
	{

		$client = new GuzzleHttp\Client(
			[
				'base_uri' => self::URL_BASE
			]
		);
		return $client;

	}

}