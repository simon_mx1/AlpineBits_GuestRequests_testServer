<?php

namespace AlpineBits\functionalTests;

use \AlpineBits\Tests\BaseFunctions;


class GetCapabilitiesTest extends BaseFunctions
{

	public function testBase()
	{

		$result = $this -> request("getCapabilities");
		$this -> assertEquals("OK:action_getVersion, action_getCapabilities, action_OTA_Read", $result["body"]);
	}

}