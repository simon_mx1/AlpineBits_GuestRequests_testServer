<?php

namespace AlpineBits\functionalTests;

use \AlpineBits\Tests\BaseFunctions;

class ValidatorTest extends BaseFunctions
{

	public function testFalseXSDandRNGValidation()
	{

		$result = $this -> request("OTA_Read:GuestRequests", "/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ_(copy).xml",
			$auth = ['Test', 'Test'], $XAlpineBitsClientID = '2017-10');

		$this -> assertEquals(200, $result['statuscode']);
		$this -> assertStringStartsWith("ERROR:", $result['body']);

	}

	public function testTrueXSDandRNGValidation()
	{

		$result = $this -> request("OTA_Read:GuestRequests", "/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml",
			$auth = ['Test', 'Test'], $XAlpineBitsClientID = '2017-10');

		$this -> assertEquals(200, $result['statuscode']);

	}

}