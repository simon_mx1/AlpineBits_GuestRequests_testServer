<?php

namespace AlpineBits\functionalTests;

use \AlpineBits\Tests\BaseFunctions;

class ResponseXMLTest extends BaseFunctions
{

	public function testXMLResponse()
	{

		$result = $this -> request("OTA_Read:GuestRequests", "/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ.xml",
			$auth = ['Test', 'Test'], $XAlpineBitsClientID = '2017-10');

		$this -> assertEquals(200, $result['statuscode']);

		file_put_contents("/home/simon/Desktop/AlpineBitsRQ/ResRetrieveRS.xml", $result['body']);
		$this -> assertEquals(file_get_contents("/home/simon/Desktop/AlpineBitsRQ/ResRetrieveRS_template.xml"), $result['body']);

		$xml = new \SimpleXMLElement($result['body']);

		$this -> assertEquals("Reserved", $xml -> ReservationsList -> HotelReservation -> attributes() -> ResStatus);
		$this -> assertEquals("2", $xml -> ReservationsList -> HotelReservation -> RoomStays -> RoomStay -> GuestCounts -> GuestCount[0] -> attributes() -> Count);
		$this -> assertEquals("299", $xml -> ReservationsList -> HotelReservation -> RoomStays -> RoomStay -> Total -> attributes() -> AmountAfterTax);
		$this -> assertEquals("4", $xml -> ReservationsList -> HotelReservation -> ResGlobalInfo -> Profiles -> ProfileInfo -> Profile -> attributes() -> ProfileType);

	}

	public function testXMLResponseNoRQ()
	{

		$result = $this -> request("OTA_Read:GuestRequests", "/home/simon/Desktop/AlpineBitsRQ/GuestRequests-OTA_ReadRQ_noRQ.xml",
			['Test', 'Test'], $XAlpineBitsClientID = '2017-10');

		file_put_contents("/home/simon/Desktop/AlpineBitsRQ/ResRetrieveRS_noRQ.xml", $result['body']);
		$this -> assertEquals(file_get_contents("/home/simon/Desktop/AlpineBitsRQ/ResRetrieveRS_noRQ_template.xml"), $result['body']);

	}
}