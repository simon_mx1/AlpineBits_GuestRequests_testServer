<?php

namespace AlpineBits\functionalTests;

use \AlpineBits\Tests\BaseFunctions;

class GetVersionTest extends BaseFunctions
{

	public function testBase()
	{

		$result = $this -> request("getVersion", "");
		$this -> assertEquals("OK:2017-10", $result["body"]);

	}

	public function testBasicAuth()
	{

		$result = $this -> request("getVersion", "", ['wronguser', 'wrongpasswd']);
		$this -> assertEquals(401, $result['statuscode']);
		$this -> assertStringStartsWith("ERROR:", $result['body']);

	}

	public function testMissingXAlpineBitsClientID()
	{

		$result = $this -> request("getVersion", "", ['Test', 'Test'], null);
		$this -> assertEquals(401, $result['statuscode']);

	}

}