<?php

namespace AlpineBits\index;

use AlpineBits\actions\Exception;
use AlpineBits\Database\DatabaseConnection;

require_once __DIR__."/../vendor/autoload.php";

$server = 'localhost:3306';
$database = 'AlpineBits';
$user = 'GuestRequests';
$password = 'TEST';


if (!($_SERVER["PHP_AUTH_USER"] == "Test" &&
	$_SERVER["PHP_AUTH_PW"] == "Test")) {

	http_response_code(401);
	echo "ERROR:authentication failed";

} elseif ($_SERVER["HTTP_X_ALPINEBITS_CLIENTID"] != "2017-10") {

	http_response_code(401);

} else {

	$action = "\\AlpineBits\\Actions\\".$_POST["action"]."\\";

	if (($pos = strpos($_POST["action"], ":")) === false)
		$action .= $_POST["action"];
	else
		$action .= substr($_POST["action"], $pos + 1);

	$action = str_replace(":", "\\", $action);


	try {

		$DB = new DatabaseConnection($server, $database, $user, $password);

		$connectionDB = $DB -> getConnection();

		$instance = new $action($connectionDB);

		if ($instance instanceof \AlpineBits\Actions\ActionInterface)
			$instance -> runAction();
		else
			throw new Exception();

	} catch (Exception $exception) {

		http_response_code(500);
		print "ERROR:internal server error";

	}
}