<?php

namespace AlpineBits\XML;


class XMLValidator
{

	private $xsd_content;
	private $rng_content;

	private $xml_doc;


	public function __construct($xsd_content, $rng_content)
	{

		$this -> xsd_content = $xsd_content;
		$this -> rng_content = $rng_content;
		$this -> xml_doc = new \DOMDocument();

	}

	public function validateXML($xml_content)
	{

		libxml_use_internal_errors(true);


		if (gettype($xml_content) == "string") {

			$this -> xml_doc -> load($xml_content);

		} else {

			$this -> xml_doc -> loadXML($xml_content);

		}


		$valid = $this -> validateXSD($this -> xsd_content) &&
			$this -> validateRNG($this -> rng_content);

		if (!$valid)
			$this -> libxml_display_errors();

		return ($valid);

	}

	private function validateXSD($xsd_content)
	{

		return $this -> xml_doc -> schemaValidate($xsd_content);

	}

	private function validateRNG($rng_content)
	{

		return $this -> xml_doc -> relaxNGValidate($rng_content);

	}

	private function libxml_display_error($error)
	{

		$return = "ERROR:";

		/* Optional for better error research
		switch ($error->level) {
			case LIBXML_ERR_WARNING:
				$return .= "$error->code: ";
				break;
			case LIBXML_ERR_ERROR:
				$return .= "$error->code: ";
				break;
			case LIBXML_ERR_FATAL:
				$return .= "$error->code: ";
				break;
		}*/

		$return .= trim($error -> message);
		if ($error -> file) {
			$return .= " in $error->file";
		}
		$return .= " on line $error->line\n";

		return $return;

	}

	private function libxml_display_errors()
	{

		$errors = libxml_get_errors();
		foreach ($errors as $error) {
			echo XMLValidator ::libxml_display_error($error);
		}
		libxml_clear_errors();

	}

}