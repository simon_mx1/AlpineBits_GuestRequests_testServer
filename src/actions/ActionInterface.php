<?php

namespace AlpineBits\Actions;

interface ActionInterface
{

    public function __construct($connectionDB);

    public function runAction();

}