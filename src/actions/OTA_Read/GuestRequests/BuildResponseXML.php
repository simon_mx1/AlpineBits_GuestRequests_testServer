<?php

namespace AlpineBits\Actions\OTA_Read\GuestRequests;

class BuildResponseXML
{

	public $OTA_ResRetrieveRS;
	public $conn;


	public function getXMLContent()
	{

		return $this -> OTA_ResRetrieveRS;

	}


	public function __construct($connectionDB, $newRequest)
	{

		$this -> conn = new ModelGuestRequests($connectionDB);


		$this -> OTA_ResRetrieveRS = new \DOMDocument('1.0', 'UTF-8');

		$this -> OTA_ResRetrieveRS -> preserveWhiteSpace = false;

		$root = $this -> OTA_ResRetrieveRS -> createElement('OTA_ResRetrieveRS');

		$this -> OTA_ResRetrieveRS -> appendChild($root);


		$root -> setAttribute('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance");

		$root -> setAttribute('xmlns', "http://www.opentravel.org/OTA/2003/05");

		$root -> setAttribute('xsi:schemaLocation', "http://www.opentravel.org/OTA/2003/05 OTA_ResRetrieveRS.xsd");

		$root -> setAttribute('Version', "7.000");


		$root -> appendChild($this -> OTA_ResRetrieveRS -> createElement('Success'));

		if ($newRequest) {

			$input_data = $this -> conn -> getDataInArray();

			$reservationsList = $this -> OTA_ResRetrieveRS -> createElement('ReservationsList');

			$root -> appendChild($reservationsList);


			$hotelReservation = $this -> OTA_ResRetrieveRS -> createElement('HotelReservation');

			$hotelReservation -> setAttribute('CreateDateTime', '2012-03-21T15:00:00+01:00');

			$hotelReservation -> setAttribute('ResStatus', "Reserved");

			$reservationsList -> appendChild($hotelReservation);


			$uniqueID = $this -> OTA_ResRetrieveRS -> createElement('UniqueID');

			$uniqueID -> setAttribute('Type', 14);

			$uniqueID -> setAttribute('ID', "6b34fe24ac2ff810");

			$hotelReservation -> appendChild($uniqueID);


			$roomStays = $this -> OTA_ResRetrieveRS -> createElement('RoomStays');
			$hotelReservation -> appendChild($roomStays);

			$roomStays -> appendChild($this -> createRoomStays($input_data));


			$resGuests = $this -> OTA_ResRetrieveRS -> createElement('ResGuests');
			$hotelReservation -> appendChild($resGuests);

			$resGuests -> appendChild($this -> createResGuests($input_data));


			$hotelReservation -> appendChild($this -> createResGlobalInfo($input_data));

		}

		$this -> OTA_ResRetrieveRS -> formatOutput = true;

	}

	private function createRoomStays($input_data)
	{

		$roomStay = $this -> OTA_ResRetrieveRS -> createElement('RoomStay');

		$roomTypes = $this -> OTA_ResRetrieveRS -> createElement('RoomTypes');
		$roomStay -> appendChild($roomTypes);

		$roomType = $this -> OTA_ResRetrieveRS -> createElement('RoomType');
		$roomTypes -> appendChild($roomType);

		$roomType -> setAttribute('RoomTypeCode', $input_data['RoomStays']['RoomTypeCode']);
		$roomType -> setAttribute('RoomClassificationCode', $input_data['RoomStays']['RoomClassificationCode']);


		$ratePlans = $this -> OTA_ResRetrieveRS -> createElement('RatePlans');
		$roomStay -> appendChild($ratePlans);

		$ratePlan = $this -> OTA_ResRetrieveRS -> createElement('RatePlan');
		$ratePlans -> appendChild($ratePlan);

		$ratePlan -> setAttribute('RatePlanCode', $input_data['RoomStays']['RatePlanCode']);

		$commisson = $this -> OTA_ResRetrieveRS -> createElement('Commission');
		$ratePlan -> appendChild($commisson);

		$commisson -> setAttribute('Percent', $input_data['RoomStays']['Percent']);

		$MealsIncluded = $this -> OTA_ResRetrieveRS -> createElement('MealsIncluded');
		$ratePlan -> appendChild($MealsIncluded);

		$MealsIncluded -> setAttribute('MealPlanIndicator', $input_data['RoomStays']['MealPlanIndicator']);
		$MealsIncluded -> setAttribute('MealPlanCodes', $input_data['RoomStays']['MealPlanCodes']);


		$guestCounts = $this -> OTA_ResRetrieveRS -> createElement('GuestCounts');
		$roomStay -> appendChild($guestCounts);


		$guestCount = $this -> OTA_ResRetrieveRS -> createElement('GuestCount');
		$guestCounts -> appendChild($guestCount);

		$guestCount -> setAttribute('Count', $input_data['RoomStays']['AdultCount']);


		for ($i = 0; $i < sizeof($input_data['RoomStays']['Child']); $i++) {

			$guestCount = $this -> OTA_ResRetrieveRS -> createElement('GuestCount');
			$guestCounts -> appendChild($guestCount);

			$guestCount -> setAttribute('Count', $input_data['RoomStays']['Child'][$i]['Count']);
			$guestCount -> setAttribute('Age', $input_data['RoomStays']['Child'][$i]['Age']);

		}


		$timeSpan = $this -> OTA_ResRetrieveRS -> createElement('TimeSpan');
		$roomStay -> appendChild($timeSpan);

		$timeSpan -> setAttribute('Start', $input_data['RoomStays']['Start']);
		$timeSpan -> setAttribute('End', $input_data['RoomStays']['End']);


		$guarantee = $this -> OTA_ResRetrieveRS -> createElement('Guarantee');
		$roomStay -> appendChild($guarantee);

		$guaranteesAccepted = $this -> OTA_ResRetrieveRS -> createElement('GuaranteesAccepted');
		$guarantee -> appendChild($guaranteesAccepted);

		$guaranteeAccepted = $this -> OTA_ResRetrieveRS -> createElement('GuaranteeAccepted');
		$guaranteesAccepted -> appendChild($guaranteeAccepted);

		$paymentCard = $this -> OTA_ResRetrieveRS -> createElement('PaymentCard');
		$guaranteeAccepted -> appendChild($paymentCard);

		$paymentCard -> setAttribute('CardCode', $input_data['RoomStays']['CardCode']);
		$paymentCard -> setAttribute('ExpireDate', $input_data['RoomStays']['ExpireDate']);


		$cardHolderName = $this -> OTA_ResRetrieveRS -> createElement('CardHolderName');
		$paymentCard -> appendChild($cardHolderName);

		$cardHolderName -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['RoomStays']['CardHolderName']));


		$cardNumber = $this -> OTA_ResRetrieveRS -> createElement('CardNumber');
		$paymentCard -> appendChild($cardNumber);

		$plainText = $this -> OTA_ResRetrieveRS -> createElement('PlainText');
		$cardNumber -> appendChild($plainText);

		$plainText -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['RoomStays']['CardNumber']));


		$total = $this -> OTA_ResRetrieveRS -> createElement('Total');
		$roomStay -> appendChild($total);

		$total -> setAttribute('AmountAfterTax', $input_data['RoomStays']['AmountAfterTax']);
		$total -> setAttribute('CurrencyCode', $input_data['RoomStays']['CurrencyCode']);


		return $roomStay;

	}

	private function createResGuests($input_data)
	{


		$resGuest = $this -> OTA_ResRetrieveRS -> createElement('ResGuest');

		$profiles = $this -> OTA_ResRetrieveRS -> createElement('Profiles');
		$resGuest -> appendChild($profiles);

		$profileInfo = $this -> OTA_ResRetrieveRS -> createElement('ProfileInfo');
		$profiles -> appendChild($profileInfo);

		$profile = $this -> OTA_ResRetrieveRS -> createElement('Profile');
		$profileInfo -> appendChild($profile);

		$customer = $this -> OTA_ResRetrieveRS -> createElement('Customer');
		$profile -> appendChild($customer);

		$customer -> setAttribute('Gender', $input_data['ResGuests']['Gender']);

		$customer -> setAttribute('BirthDate', $input_data['ResGuests']['BirthDate']);

		$customer -> setAttribute('Language', $input_data['ResGuests']['Language']);


		$personName = $this -> OTA_ResRetrieveRS -> createElement('PersonName');
		$customer -> appendChild($personName);


		$namePrefix = $this -> OTA_ResRetrieveRS -> createElement('NamePrefix');
		$personName -> appendChild($namePrefix);

		$namePrefix -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['PersonName']['NamePrefix']));


		$givenName = $this -> OTA_ResRetrieveRS -> createElement('GivenName');
		$personName -> appendChild($givenName);

		$givenName -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['PersonName']['GivenName']));


		$surname = $this -> OTA_ResRetrieveRS -> createElement('Surname');
		$personName -> appendChild($surname);

		$surname -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['PersonName']['Surname']));


		$nameTitle = $this -> OTA_ResRetrieveRS -> createElement('NameTitle');
		$personName -> appendChild($nameTitle);

		$nameTitle -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['PersonName']['NameTitle']));


		for ($i = 0; $i < sizeof($input_data['ResGuests']['Telephone']); $i++) {

			$telephone = $this -> OTA_ResRetrieveRS -> createElement('Telephone');
			$customer -> appendChild($telephone);

			$telephone -> setAttribute('PhoneTechType', $input_data['ResGuests']['Telephone'][$i]['PhoneTechType']);
			$telephone -> setAttribute('PhoneNumber', $input_data['ResGuests']['Telephone'][$i]['PhoneNumber']);

		}


		$email = $this -> OTA_ResRetrieveRS -> createElement('Email');
		$customer -> appendChild($email);

		$email -> setAttribute('Remark', $input_data['ResGuests']['Email']['News']);
		$email -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['Email']['Address']));

		$address = $this -> OTA_ResRetrieveRS -> createElement('Address');
		$customer -> appendChild($address);


		$address -> setAttribute('Remark', $input_data['ResGuests']['Address']['Catalog']);

		$addressLine = $this -> OTA_ResRetrieveRS -> createElement('AddressLine');
		$address -> appendChild($addressLine);

		$addressLine -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['Address']['AddressLine']));


		$cityName = $this -> OTA_ResRetrieveRS -> createElement('CityName');
		$address -> appendChild($cityName);

		$cityName -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['Address']['CityName']));


		$postalCode = $this -> OTA_ResRetrieveRS -> createElement('PostalCode');
		$address -> appendChild($postalCode);

		$postalCode -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGuests']['Address']['PostalCode']));


		$countryName = $this -> OTA_ResRetrieveRS -> createElement('CountryName');
		$address -> appendChild($countryName);

		$countryName -> setAttribute('Code', $input_data['ResGuests']['Address']['CountryName']);

		//var_dump($resGuest);
		return $resGuest;

	}

	private function createResGlobalInfo($input_data)
	{

		$resGlobalInfo = $this -> OTA_ResRetrieveRS -> createElement('ResGlobalInfo');

		$comments = $this -> OTA_ResRetrieveRS -> createElement('Comments');
		$resGlobalInfo -> appendChild($comments);

		$comment = $this -> OTA_ResRetrieveRS -> createElement('Comment');
		$comments -> appendChild($comment);


		$comment -> setAttribute('Name', $input_data['ResGlobalInfo']['Comments']['Name']);

		for ($j = 0; $j < sizeof($input_data['ResGlobalInfo']['Comments']) - 1; $j++) {

			$listItem = $this -> OTA_ResRetrieveRS -> createElement('ListItem');
			$comment -> appendChild($listItem);
			$listItem -> setAttribute('ListItem', $input_data['ResGlobalInfo']['Comments'][$j]['ListItem']);
			$listItem -> setAttribute('Language', $input_data['ResGlobalInfo']['Comments'][$j]['Language']);
			$listItem -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Comments'][$j]['Item']));

		}


		$comment = $this -> OTA_ResRetrieveRS -> createElement('Comment');
		$comments -> appendChild($comment);

		$comment -> setAttribute('Name', $input_data['ResGlobalInfo']['TextComment']['Name']);

		$text = $this -> OTA_ResRetrieveRS -> createElement('Text');
		$comment -> appendChild($text);

		$text -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['TextComment']['Text']));


		$cancelPenalties = $this -> OTA_ResRetrieveRS -> createElement('CancelPenalties');
		$resGlobalInfo -> appendChild($cancelPenalties);

		$cancelPenalty = $this -> OTA_ResRetrieveRS -> createElement('CancelPenalty');
		$cancelPenalties -> appendChild($cancelPenalty);

		$penaltyDescription = $this -> OTA_ResRetrieveRS -> createElement('PenaltyDescription');
		$cancelPenalty -> appendChild($penaltyDescription);

		$text = $this -> OTA_ResRetrieveRS -> createElement('Text');
		$penaltyDescription -> appendChild($text);

		$text -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['CancelPenalty']));


		$hotelReservationIDs = $this -> OTA_ResRetrieveRS -> createElement('HotelReservationIDs');
		$resGlobalInfo -> appendChild($hotelReservationIDs);

		$hotelReservationID = $this -> OTA_ResRetrieveRS -> createElement('HotelReservationID');
		$hotelReservationIDs -> appendChild($hotelReservationID);

		$hotelReservationID -> setAttribute('ResID_Type', $input_data['ResGlobalInfo']['HotelReservationID']['ResID_Type']);
		$hotelReservationID -> setAttribute('ResID_Value', $input_data['ResGlobalInfo']['HotelReservationID']['ResID_Value']);
		$hotelReservationID -> setAttribute('ResID_Source', $input_data['ResGlobalInfo']['HotelReservationID']['ResID_Source']);
		$hotelReservationID -> setAttribute('ResID_SourceContext', $input_data['ResGlobalInfo']['HotelReservationID']['ResID_SourceContext']);


		$profiles = $this -> OTA_ResRetrieveRS -> createElement('Profiles');
		$resGlobalInfo -> appendChild($profiles);

		$profileInfo = $this -> OTA_ResRetrieveRS -> createElement('ProfileInfo');
		$profiles -> appendChild($profileInfo);

		$profile = $this -> OTA_ResRetrieveRS -> createElement('Profile');
		$profileInfo -> appendChild($profile);

		$profile -> setAttribute('ProfileType', $input_data['ResGlobalInfo']['Profiles']['ProfileType']);

		$companyInfo = $this -> OTA_ResRetrieveRS -> createElement('CompanyInfo');
		$profile -> appendChild($companyInfo);


		$companyName = $this -> OTA_ResRetrieveRS -> createElement('CompanyName');
		$companyInfo -> appendChild($companyName);

		$companyName -> setAttribute('Code', $input_data['ResGlobalInfo']['Profiles']['CompanyName']['Code']);
		$companyName -> setAttribute('CodeContext', $input_data['ResGlobalInfo']['Profiles']['CompanyName']['CodeContext']);
		$companyName -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Profiles']['CompanyName']['Name']));


		$addressInfo = $this -> OTA_ResRetrieveRS -> createElement('AddressInfo');
		$companyInfo -> appendChild($addressInfo);

		$addressLine = $this -> OTA_ResRetrieveRS -> createElement('AddressLine');
		$addressInfo -> appendChild($addressLine);

		$addressLine -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Profiles']['Address']['AddressLine']));


		$cityName = $this -> OTA_ResRetrieveRS -> createElement('CityName');
		$addressInfo -> appendChild($cityName);

		$cityName -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Profiles']['Address']['CityName']));


		$postalCode = $this -> OTA_ResRetrieveRS -> createElement('PostalCode');
		$addressInfo -> appendChild($postalCode);

		$postalCode -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Profiles']['Address']['PostalCode']));


		$countryName = $this -> OTA_ResRetrieveRS -> createElement('CountryName');
		$addressInfo -> appendChild($countryName);

		$countryName -> setAttribute('Code', $input_data['ResGlobalInfo']['Profiles']['Address']['CountryName']);


		$telephoneInfo = $this -> OTA_ResRetrieveRS -> createElement('TelephoneInfo');
		$companyInfo -> appendChild($telephoneInfo);

		$telephoneInfo -> setAttribute('PhoneTechType', $input_data['ResGlobalInfo']['Profiles']['Telephone']['PhoneTechType']);
		$telephoneInfo -> setAttribute('PhoneNumber', $input_data['ResGlobalInfo']['Profiles']['Telephone']['PhoneNumber']);


		$email = $this -> OTA_ResRetrieveRS -> createElement('Email');
		$companyInfo -> appendChild($email);

		$email -> appendChild($this -> OTA_ResRetrieveRS -> createTextNode($input_data['ResGlobalInfo']['Profiles']['Email']));


		$resGlobalInfo -> appendChild($this -> OTA_ResRetrieveRS -> createElement('BasicPropertyInfo'));

		return $resGlobalInfo;

	}


}
