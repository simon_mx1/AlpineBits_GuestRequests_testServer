<?php

namespace AlpineBits\Actions\OTA_Read\GuestRequests;


use AlpineBits\Actions\Exception;
use \AlpineBits\XML\XMLValidator;


class GuestRequests implements \AlpineBits\Actions\ActionInterface
{

	/** @var \PDO $database */
	public $connectionDB;

	public function __construct($connectionDB)
	{

		$this -> connectionDB = $connectionDB;

	}


	public function runAction()
	{
		$validator = new XMLValidator('/home/simon/Desktop/AlpineBitsRQ/alpinebits.xsd', '/home/simon/Desktop/AlpineBitsRQ/alpinebits.rng');

		$valid = $validator -> validateXML($_POST["request"]);

		if ($valid) {

			$Extractor = new RQExtractor($_POST["request"]);

			$date = $Extractor -> getStartDate();

			$date = str_replace("T", " ", $date);

			$datesAdded = $this -> connectionDB -> query("SELECT CreateDateTime FROM AlpineBits.GuestRequests_HotelReservation") -> fetchAll(\PDO::FETCH_ASSOC);
			$retrievedAll = $this -> connectionDB -> query("SELECT Retrieved FROM AlpineBits.GuestRequests_HotelReservation") -> fetchAll(\PDO::FETCH_ASSOC);



			$newRequests = (!$retrievedAll[0]['Retrieved']) && (strtotime($date) < strtotime($datesAdded[0]["CreateDateTime"]));


			$xmlResponse = new BuildResponseXML($this -> connectionDB, $newRequests);

			$validResponse = $validator -> validateXML($xmlResponse -> getXMLContent() -> saveXML());

			if (!$validResponse) {

				throw new Exception();

			} else {

				http_response_code(200);
				//$this->connectionDB -> query("UPDATE AlpineBits.GuestRequests_HotelReservation SET Retrieved = 1
				//WHERE AlpineBits.GuestRequests_HotelReservation.Retrieved = 0");
				echo $xmlResponse -> getXMLContent() -> saveXML();

			}

		} else {

			http_response_code(200);

		}
	}
}