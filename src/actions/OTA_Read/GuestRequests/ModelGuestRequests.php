<?php

namespace AlpineBits\Actions\OTA_Read\GuestRequests;


//use AlpineBits\Database\DatabaseConnection;

class ModelGuestRequests
{
	/** @var \PDO $database */
	private $database; //database object


	public function __construct($connectionDB)
	{

		$this -> database = $connectionDB;

	}


	public function getDataInArray()
	{

		$requestNumber = 1;

		$roomStay = $this -> database -> query("SELECT * FROM AlpineBits.GuestRequests_RoomStay
                                            WHERE AlpineBits.GuestRequests_RoomStay.ResGuest_ID = $requestNumber") -> fetchAll(\PDO::FETCH_ASSOC);

		$comments = $this -> database -> query("SELECT * FROM AlpineBits.GuestRequests_Comment
                                            WHERE AlpineBits.GuestRequests_Comment.ResGlobalInfo_ID = $requestNumber") -> fetchAll(\PDO::FETCH_ASSOC);

		$guestCount = $this -> database -> query("SELECT * FROM AlpineBits.GuestRequests_GuestCount
                                            WHERE AlpineBits.GuestRequests_GuestCount.RoomStay_ID = $requestNumber") -> fetchAll(\PDO::FETCH_ASSOC);

		$resGuests = $this -> database -> query("SELECT * FROM AlpineBits.GuestRequests_ResGuest
                                            WHERE AlpineBits.GuestRequests_ResGuest.ID=  $requestNumber") -> fetchAll(\PDO::FETCH_ASSOC);

		$resGlobalInfo = $this -> database -> query("SELECT * FROM GuestRequests_ResGlobalInfo 
                                            WHERE AlpineBits.GuestRequests_ResGlobalInfo.ID = $requestNumber") -> fetchAll(\PDO::FETCH_ASSOC);

		$roomStaysNumber = 0;
		$resGuestNumber = $requestNumber - 1;
		$resGlobalInfoNumber = $requestNumber - 1;

		if ($roomStay[$roomStaysNumber]["DateAdded"])
		$input_data = array(

			'RoomStays' => array(

				'RoomTypeCode' => $roomStay[$roomStaysNumber]["RoomTypeCode"],
				'RoomClassificationCode' => $roomStay[$roomStaysNumber]["RoomClassificationCode"],
				'RatePlanCode' => $roomStay[$roomStaysNumber]["RatePlanCode"],
				'Percent' => $roomStay[$roomStaysNumber]["Percent"],
				'MealPlanIndicator' => $roomStay[$roomStaysNumber]["MealPlanIndicator"],
				'MealPlanCodes' => $this -> addMealPlan($roomStay[$roomStaysNumber]["MealPlanCodes"]),
				'AdultCount' => $guestCount[0]["Count"],
				'Child' => array(/*see below*/),
				'Start' => $roomStay[$roomStaysNumber]["TimeSpanStart"],
				'End' => $roomStay[$roomStaysNumber]["TimeSpanEnd"],
				'CardCode' => $roomStay[$roomStaysNumber]["CardCode"],
				'ExpireDate' => $roomStay[$roomStaysNumber]["CardExpireDate"],
				'CardHolderName' => $roomStay[$roomStaysNumber]["CardholderName"],
				'CardNumber' => $roomStay[$roomStaysNumber]["CardNumber"],
				'AmountAfterTax' => $roomStay[$roomStaysNumber]["AmountAfterTax"],
				'CurrencyCode' => $roomStay[$roomStaysNumber]["CurrencyCode"]

			),

			'ResGuests' => array(

				'Gender' => $resGuests[$resGuestNumber]["Gender"],
				'BirthDate' => $resGuests[$resGuestNumber]["BirthDate"],
				'Language' => $resGuests[$resGuestNumber]["Language"],
				'PersonName' => array(

					'NamePrefix' => $resGuests[$resGuestNumber]["NamePrefix"],
					'GivenName' => $resGuests[$resGuestNumber]["GivenName"],
					'Surname' => $resGuests[$resGuestNumber]["Surname"],
					'NameTitle' => $resGuests[$resGuestNumber]["NameTitle"]

				),

				'Telephone' => array(

					array(
						'PhoneTechType' => $resGuests[$resGuestNumber]["Phone_Voice"] != null ? 1 : null,
						'PhoneNumber' => $resGuests[$resGuestNumber]["Phone_Voice"],
					),
					array(
						'PhoneTechType' => $resGuests[$resGuestNumber]["Phone_Fax"] != null ? 3 : null,
						'PhoneNumber' => $resGuests[$resGuestNumber]["Phone_Fax"],
					),
					array(
						'PhoneTechType' => $resGuests[$resGuestNumber]["Phone_Mobile"] != null ? 5 : null,
						'PhoneNumber' => $resGuests[$resGuestNumber]["Phone_Mobile"],
					),

				),

				'Email' => array(

					'News' => $resGuests[$resGuestNumber]["News"] ? 'newsletter:yes' : 'newsletter:no',
					'Address' => $resGuests[$resGuestNumber]["Email"]

				),

				'Address' => array(

					'Catalog' => $resGuests[$resGuestNumber]["Catalog"] ? 'catalog:yes' : 'catalog:no',
					'AddressLine' => $resGuests[$resGuestNumber]["AddressLine"],
					'CityName' => $resGuests[$resGuestNumber]["CityName"],
					'PostalCode' => $resGuests[$resGuestNumber]["PostalCode"],
					'CountryName' => $resGuests[$resGuestNumber]["CountryName"]

				)

			),

			'ResGlobalInfo' => array(

				'Comments' => array(/*see below*/),
				'TextComment' => array(/*see below*/),
				'CancelPenalty' => $resGlobalInfo[$resGlobalInfoNumber]["CancelPenalty"],
				'HotelReservationID' => array(

					'ResID_Type' => $resGlobalInfo[$resGlobalInfoNumber]["ResID_Type"],
					'ResID_Value' => $resGlobalInfo[$resGlobalInfoNumber]["ResID_Value"],
					'ResID_Source' => $resGlobalInfo[$resGlobalInfoNumber]["ResID_Source"],
					'ResID_SourceContext' => $resGlobalInfo[$resGlobalInfoNumber]["ResID_SourceContext"]

				),

				'Profiles' => array(

					'ProfileType' => $this->profileType($resGlobalInfo[$resGlobalInfoNumber]["ProfileType"]),
					'CompanyName' => array(

						'Code' => $resGlobalInfo[$resGlobalInfoNumber]["CompanyCode"],
						'CodeContext' => $resGlobalInfo[$resGlobalInfoNumber]["CompanyCodeContext"],
						'Name' => $resGlobalInfo[$resGlobalInfoNumber]["CompanyName"]

					),

					'Address' => array(

						'AddressLine' => $resGlobalInfo[$resGlobalInfoNumber]["AddressLine"],
						'CityName' => $resGlobalInfo[$resGlobalInfoNumber]["CityName"],
						'PostalCode' => $resGlobalInfo[$resGlobalInfoNumber]["PostalCode"],
						'CountryName' => $resGlobalInfo[$resGlobalInfoNumber]["CountryName"]

					),

					'Telephone' => array(

						'PhoneTechType' => $resGlobalInfo[$resGlobalInfoNumber]["Phone_Voice"] != null ? 1 : null,
						'PhoneNumber' => $resGlobalInfo[$resGuestNumber]["Phone_Voice"]

					),

					'Email' => $resGlobalInfo[$resGlobalInfoNumber]["Email"]

				)

			)

		);

		//adding the children

		$input_data = $this -> addChildren($guestCount, $input_data);


		//adding the comments

		$input_data = $this -> addComments($comments, $input_data);


		return $input_data;
	}


	public function addChildren($guestCount, $input_data)
	{

		for ($i = 1; $i < sizeof($guestCount); $i++) {
			array_push($input_data["RoomStays"]["Child"],
				array('Count' => $guestCount[$i]["Count"],
					'Age' => $guestCount[$i]["Age"]));
		}

		return $input_data;

	}


	public function addComments($comments, $input_data)
	{

		for ($i = 0; $i < sizeof($comments); $i++) {

			if ($comments[$i]["Comment_Name"] == "included services") {

				if (!array_key_exists("Name", $input_data["ResGlobalInfo"]["Comments"])) {
					$input_data["ResGlobalInfo"]["Comments"] = array("Name" => $comments[$i]["Comment_Name"]);
				}

				array_push($input_data["ResGlobalInfo"]["Comments"], array(

					'ListItem' => $comments[$i]["ListItem"],
					'Language' => $comments[$i]["Language"],
					'Item' => $comments[$i]["Comment"]

				));

			} elseif ($comments[$i]["Comment_Name"] == "customer comment") {

				$input_data["ResGlobalInfo"]["TextComment"] = array(

					'Name' => $comments[$i]["Comment_Name"],
					'Text' => $comments[$i]["Comment"]);

			}

		}

		return $input_data;

	}


	public function addMealPlan($mealPlan)
	{

		switch ($mealPlan) {

			case "all inclusive":
				return 1;
				break;

			case "bed and breakfast":
				return 3;
				break;

			case "full board":
				return 10;
				break;

			case "half board":
				return 12;
				break;

			case "room only":
				return 14;
				break;

			default:
				return 14;
				break;

		}
	}

	public function profileType($type) {

		switch ($type) {

			case 'Travel Agent':
				return 4;
				break;

			default:
				return 4;
				break;

		}

	}

}
