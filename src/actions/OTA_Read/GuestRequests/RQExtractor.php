<?php

namespace AlpineBits\Actions\OTA_Read\GuestRequests;

class RQExtractor
{

	private $xml;

	public function __construct($xmlContent)
	{

		$this -> xml = simplexml_load_file($xmlContent);

	}

	public function getHotelCode()
	{

		return (string)$this -> xml -> ReadRequests -> HotelReadRequest -> attributes() -> HotelCode;

	}

	public function getHotelName()
	{

		return (string)$this -> xml -> ReadRequests -> HotelReadRequest -> attributes() -> HotelName;

	}

	public function getStartDate()
	{

		return (string)$this -> xml -> ReadRequests -> HotelReadRequest -> SelectionCriteria -> attributes() -> Start;

	}

}